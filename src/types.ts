import {AxiosRequestConfig} from 'axios';

export type Locale = 'cs' | 'en';

export enum Creator {
    DIRECTOR = "Režie",
    WRITER = "Scénář",
    MUSIC = "Hudba",
    CAST = "Hrají",
    CAMERA = "Kamera",
    AUTHOR = "Tvůrci"
}

export enum CSFDType {
    TV_SHOW = "TV seriál",
    TV_SERIES = "TV pořad",
    SEASON = "série",
    EPISODE = "epizoda",
    CONCERT = "koncert",
    FILM = "film",
    TV_FILM = "TV film",
}

export enum PremiereType {
    TV = "tv",
    BLU_RAY = "blu-ray",
    CINEMA = "cinema",
    DVD = "dvd",
    VOD = "vod",
}

export type Premieres = Record<string, Partial<Record<PremiereType, Date>>>;

export interface CSFDItemTitle {
    language?: string;
    title: string;
}

export interface CSFDItemTrailer {
    name: string;
    language: string | undefined;
    src: string;
    quality: number | undefined;
    subtitles: { src: string; language: string | undefined; }[];
}

export type HttpConfig = AxiosRequestConfig;

export interface CSFDConfig {
    http?: HttpConfig;
    chunkSize?: number;
    proxyUrl?: string;
}

export interface CSFDItemProps {
    ids: {
        csfd: number;
        imdb?: string;
    };
    parent_id?: number;
    children_ids: number[];
    main_title: string;
    titles: CSFDItemTitle[];
    year: number;
    duration?: number;
    mediaType: CSFDType;
    poster?: string;
    season?: number;
    episode?: number;
    plot?: string;
    rating?: number;
    votes?: number;
    origin?: string[];
    premieres?: Premieres;
    genre: string[];
    director: string[];
    writer: string[];
    cast: string[];
    author: string[];
    music: string[];
    camera: string[];
    trailers: CSFDItemTrailer[];
    certification?: string;
    children?: CSFDItemProps[];
}


export interface CSFDSearchResponseMediaItem {
    name: string;
    search_name: string;
    year: number;
    rating_icon_class: string;
    link: string;
}

export type CSFDSearchResponseMovieItem = CSFDSearchResponseMediaItem;
export type CSFDSearchResponseShowItem = CSFDSearchResponseMediaItem;
export interface CSFDSearchResponseUserItem {
    nick: string;
    fullname: string;
    link: string;
}
export interface CSFDSearchResponseCreatorItem {
    search_name: string;
    name: string;
    link: string;
}

export interface CSFDSearchResponse {
    users: CSFDSearchResponseUserItem[];
    films: CSFDSearchResponseMovieItem[];
    series: CSFDSearchResponseShowItem[];
    creators: CSFDSearchResponseCreatorItem[];
}
