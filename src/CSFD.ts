import {CSFDItem} from './CSFDItem';
import {CSFD_URL_SEARCH, DEFAULT_CONFIG} from './const';
import {CSFDSearchItem} from './CSFDSearchItem';
import {CSFDConfig, CSFDSearchResponse, Locale} from './types';
import {Config, Fetch} from '@media-info/fetch';

export class CSFD {
    locale: Locale;
    config: CSFDConfig & Config;
    fetcher: Fetch;

    constructor(locale: Locale, config: CSFDConfig = DEFAULT_CONFIG) {
        this.locale = locale;
        this.config = {...DEFAULT_CONFIG, ...config};
        this.fetcher = new Fetch(this.config);
    }

    get(id: number | string) {
        return new CSFDItem(id, this.locale, this.config);
    };

    fetch(id: number | string, children?: boolean) {
        return new CSFDItem(id, this.locale, this.config).fetch(children);
    };

    async search(value: string) {
        const res = await this.fetcher.req<CSFDSearchResponse>(CSFD_URL_SEARCH(value), {
            headers: {'X-Requested-With': 'XMLHttpRequest'}
        });

        return new CSFDSearchItem(this, res.data);
    }
}
