[![npm version](https://badge.fury.io/js/@media-info%2Fcsfd-api.svg)](https://badge.fury.io/js/@media-info%2Fcsfd-api)

A simple **UMD** package to fetch data about movies and shows recursively from **[ČSFD](https://csfd.cz)** for both
the **browser** and **Node.js**.

# Table of Contents

- [Install](#install)
- [Example](#example)
- API
    - [CSFD](#csfd)
    - [CSFDItem](#csfditem)
        - [Available properties](#available-properties)

# Install

    $ npm install @media-info/csfd-api

# Example

```javascript
import CSFD from '@media-info/csfd-api';

const csfd = new CSFD('cs');
const csfdItem = await csfd.fetch(1);
console.log(data);
```

---

# API

You can fetch data by using CSFD API wrapper or by accessing CSFDItem class object directly.

## CSFD

### new CSFD(locale, [config])

- `locale` (`string`, values: `cs` `en`): translates **genres** and **countries** to set language
- `config` (`object`, `optional`, default `{}`): allows you to
  configure [axios](https://www.npmjs.com/package/axios#request-config) which is internally used to fetch data. You can
  also set `proxyUrl` to avoid CORS issues using a server as proxy. It appends URL path after provided value.

Creates CSFD class object using provided config

```javascript

const csfd = new CSFD('cs', {
    http: {
        headers: {'X-Requested-With': 'XMLHttpRequest'}
    },
    // example internal call: https://example.proxy/csfd/film/69266-tom-a-jerry/prehled/
    proxyUrl: 'https://example.proxy/csfd',
    chunkSize: 10,
});
```

### get(id)

Get CSFDItem class object using an ID.

```javascript
const csfdItem = csfd.get(1);
await csfdItem.fetch();
```

### fetch(id, [children])

Get CSFD data object directly using an ID.

```javascript
const csfdData = await csfd.fetch(1);
```

### search(value)

Get CSFDSearchItem class object using a ČSFD search.

```javascript
const csfdSearchItem = await csfd.search('apolo');
const csfdItems = await csfdSearchItem.fetchAll();
```

## CSFDSearchItem

### fetchAll([children])

Fetches all available data for every search result item and returns them as an object

## CSFDItem

### fetch([children])

Fetches all available data and returns them as an object

- `children` (`boolean`, `optional`, default `true`): children are fetched concurrently in chunks to increase speed
  dramatically. Default chunk size is **10**.

  > **NOTE:** *It takes a lot of time when fetching big tv show - each item requires **2** HTTP requests*.

  Example
  ```javascript
  csfdItem.fetch(false).then(response => {});
  ```
  Response
  ```json
  {
    "ids": {
      "csfd": 2294
    },
    "children_ids": [],
    "main_title": "Vykoupení z věznice Shawshank",
    "titles": [
      { 
        "language": "en", 
        "title": "The Shawshank Redemption", 
        "country": "US" 
      },
      { 
        "language": "sk",
        "title": "Vykúpenie z väznice Shawshank",
        "country": "SK" 
      },
      {
        "language": "en",
        "title": "The Shawshank Redemption",
        "country": "NZ"
      }
    ],
    "year": 1994,
    "duration": 8520,
    "mediaType": "film",
    "poster": "//image.pmgstatic.com/files/images/film/posters/162/505/162505167_735db9.jpg",
    "plot": "Mladý bankovní manažer Andy Dufresne (Tim R...",
    "rating": 9.5,
    "votes": 107035,
    "origin": [
      "US"
    ],
    "premieres": {
      "CZ": {
        "cinema": "1995-07-06T00:00:00.000Z",
        "dvd": "2003-03-31T00:00:00.000Z",
        "blu-ray": "2016-11-30T00:00:00.000Z"
      },
      "US": {
        "cinema": "1994-10-14T00:00:00.000Z"
      }
    },
    "genre": [
      "Drama",
      "Crime"
    ],
    "director": [
      "Frank Darabont"
    ],
    "writer": [
      "Frank Darabont"
    ],
    "cast": [
      "Tim Robbins",
      "Morgan Freeman"
    ],
    "author": [],
    "music": [
      "Thomas Newman"
    ],
    "camera": [
      "Roger Deakins"
    ],
    "trailers": [
      {
        "name": "Trailer 1",
        "language": "en",
        "src": "//video.pmgstatic.com/files/videos/008/486/8486616/157727807_28ae07.mp4",
        "quality": 360,
        "subtitles": [
          {
            "src": "//video.pmgstatic.com/files/subtitles/008/500/8500445_08847d.vtt",
            "language": "cs"
          }
        ]
      }
    ],
    "children": []
  }
  ```

### Available properties

You can access each property separately.

##### get cast()

##### get children()

##### get director()

##### get music()

##### get writer()

##### get camera()

##### get author()

##### get genre()

##### get rating()

##### get mainTitle()

##### get titles()

##### *async* get trailer()

##### get imdb()

##### get mediaType()

##### get votes()

##### get plot()

##### get origin()

##### get year()

##### get duration()

##### get poster()

##### get premieres()

##### get certification()

##### get parent()

##### get childrenIds()

##### get season()

##### get episode()

> **NOTE**: you have to load children data before you can access their properties

Example:

```javascript
csfdItem.cast.join(', ');
console.log(csfdItem.areChildrenLoaded) // false
csfdItem.children.find(child => child.season === 1); // undefined

await csfdItem.fetchChildren();
console.log(csfdItem.areChildrenLoaded) // true
csfdItem.children.find(child => child.season === 1); // CSFDItem
```
